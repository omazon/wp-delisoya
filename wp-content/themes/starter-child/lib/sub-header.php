<?php 
    $output = ''; 
    $sub_img = array();
    $categories = get_terms('category');
    global $post;
    if(!function_exists('starterpro_call_sub_header')){
        function starterpro_call_sub_header(){
            
            $banner_img = get_theme_mod( 'sub_header_banner_img', false );
            $banner_color = get_theme_mod( 'sub_header_banner_color', '#333' );

            if( $banner_img ){
                $output = 'style="background-image:url('.esc_url( $banner_img ).');background-size: cover;background-position: 50% 50%;"';
                return $output;
            }else{
                $output = 'style="background-color:'.esc_attr( $banner_color ).';"';
                return $output;
            }
        }
    }
    
    if( isset($post->post_name) ){
        if(!empty($post->ID)){
            if(function_exists('rwmb_meta')){

                $image_attached = esc_attr(get_post_meta( $post->ID , 'themeum_subtitle_images', true ));
                if(!empty($image_attached)){
                    $sub_img = wp_get_attachment_image_src( $image_attached , 'blog-full'); 
                    $output = 'style="background-image:url('.esc_url($sub_img[0]).');background-size: cover;background-position: 50% 50%;"';
                    if(empty($sub_img[0])){
                        $output = 'style="background-color:'.esc_attr(get_post_meta(get_the_ID(),"themeum_subtitle_color",true)).';"';
                        if( get_post_meta(get_the_ID(),"themeum_subtitle_color",true ) == ''){
                            $output = starterpro_call_sub_header();
                        }
                    }
                }else{
                    if( get_post_meta(get_the_ID(),"themeum_subtitle_color",true) != "" ){
                        $output = 'style="background-color:'.esc_attr(get_post_meta(get_the_ID(),"themeum_subtitle_color",true)).';"';
                    }else{
                        $output = starterpro_call_sub_header();
                    }
                }
            }else{
                $output = starterpro_call_sub_header();
            } 
        }else{
            $output = starterpro_call_sub_header();
        }
    }else{
        $output = starterpro_call_sub_header();
    }

?>
<?php if (!is_front_page()) { ?>
<?php
    $search_cls = (is_search() && (get_query_var('post_type') == 'hotel' || get_query_var('post_type') == 'package' || get_query_var('post_type') == 'vehicle')) ?  'thm-tk-search-page-header' : '';
?>
<div class="subtitle-cover sub-title text-center <?php echo esc_attr($search_cls); ?>" <?php print $output;?>>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                    <?php

                        if (get_theme_mod( 'sub_header_title_enable', true )) {
  
                            global $wp_query; 

                            if(isset($wp_query->queried_object->name)){
                                if($wp_query->queried_object->name != ''){
                                    echo '<h2 class="page-leading" data-category="'.$wp_query->queried_object->term_id.'">'.$wp_query->queried_object->name.'</h2>';
                                }else{
                                    echo '<h2 class="page-leading">'.get_the_title().'</h2>';
                                }
                            }else{
                                 
                                if( is_search() ){
                                    $first_char = esc_html__('Search','starter');
                                    echo '<h2 class="page-leading">'.$first_char.'</h2>';
                                }

                                else if( is_home() ){
                                        echo '<h2 class="gotham-bold" data-category="0">';
                                        wp_title('');
                                        echo '</h2>';
                                    }

                                    else if( is_single() && get_post_type() == 'project'){
                                   echo '<h2 class="page-leading">'.get_the_title().'</h2>';
                                } 
                                else if( is_single()){
                                   echo '<h2 class="page-leading">'.esc_html__('Blog','starter').'</h2>';
                                } 

                                else{
                                    echo '<h2 class="page-leading">'.get_the_title().'</h2>';
                                }
                            }
                        }?>
                        <div class="flex jc-center font-30 gotham-bold mt-10">
                        <?php
                        if(!is_single()){
	                        foreach ($categories as $category){
		                        printf('                            
                                <span class="mx-5" style="color: %s;">%s</span>                                                     
                            ',get_field('color_de_la_categoria','category_'.$category->term_id),$category->name);
	                        }
                        }else{
                            printf('                            
                            <span class="mx-5" style="color: %s;">%s</span>                                                     
                        ',get_field('color_de_la_categoria','category_'.get_the_category()[0]->term_id),get_the_category()[0]->name);
                        }
                    ?>
                        </div>
                <!-- if ( is_single() && get_theme_mod( 'breadcrumb_enable', true )) {
					starterpro_breadcrumbs();
				} -->
                
            </div>
        </div>
    </div>
</div>
<?php
    if(!is_single()){
	    printf('
        <div class="category-sub-header flex ai-center jc-center fw-wrap">
            <a class="font-20 dark-grey-text" data-category="0" data-color="#9ECEE8" style="border-bottom: 0px solid #9ECEE8" href="%s">Todos</a>',get_permalink(get_option('page_for_posts')));
	    foreach ($categories as $category){
	        $color = get_field('color_de_la_categoria','category_'.$category->term_id);
		    printf('        
        <a class="font-20 dark-grey-text" style="border-bottom: 0px solid %s;" data-color="%s" data-category="%s" href="%s">%s</a>        
    ',
			    $color,
			    $color,
			    $category->term_id,
			    get_category_link($category->term_id),
			    $category->name);
	    }
	    echo '</div>';
    }else{
        printf('<div class="category-sub-header"></div>');
    }
?>
    <!--/.sub-title-->
<?php } ?>
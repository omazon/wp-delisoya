<style>
    .testimonio{
        color: <?php echo $variable['color']?>;
    }
</style>
<section class="testimonio">
    <div class="owl-carousel text-center">
		<?php
		if(have_rows('testimonios','option')):
			while(have_rows('testimonios','option')):the_row();
				printf('
		<div class="single-testimonial font-30 line-30">
			<div class="contenido mb-50 flex ai-center">
				<img class="width-20 as-start hidden-xs" src="%s">
				<div class="gotham-book">
					%s
				</div>
				<img class="width-20 as-end hidden-xs" src="%s">
			</div>
			<div class="gotham-medium">
				%s
			</div>
			<div class="gotham-book">
				%s
			</div>
		</div>',
					get_stylesheet_directory_uri().'/img/comillaizquierda.png',
					get_sub_field('contenido'),
					get_stylesheet_directory_uri().'/img/comilladerecha.png',
					get_sub_field('nombre'),
					get_sub_field('cargo'));
			endwhile;
		endif;
		?>
    </div>
</section>

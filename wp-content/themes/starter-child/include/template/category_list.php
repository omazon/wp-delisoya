<?php
if(!function_exists('get_args')){
	function get_args($value=''){
		$args = array(
			'post_per_page' => 3,
			'category' => $value,
			'order' => 'DESC',
			'post_status'      => 'publish',
		);
		return $args;
	}
}
if(!function_exists('get_content')){
	function get_content($thumb,$tag,$color = '#08255A',$category,$link,$title){
		return printf('<div class="col-sm-4 list_post white-text">
                    <div class="img-post" style="background: url(%s)">
                        <img style="position:absolute;left: 30px" src="%s">
                    </div>
                    <div class="contenido" style="background: %s">
                        <span class="categoria gotham-medium">%s</span>
                        <a class="detalles white-text" href="%s">                       
                            <span class="titulo gotham-bold">%s</span>
                            <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>
                        </a>      
                    </div>                                  
                </div>',
            $thumb,
            $tag,
            $color,
            $category,
            $link,
            $title = (strlen($title)>=21) ? substr($title,0,21).'...' : $title);
	};
}

$categories = get_terms('category');
$post_array = get_posts(get_args());
?>
<ul class="nav nav-tabs nav-post">
    <li class="active">
        <a href="#todos" data-toggle="tab" style="border-bottom: 0px solid #9ECEE8">Todas</a>
    </li>
	<?php
    foreach ($categories as $category){
	    printf('
            <li>
                <a href="#%s" data-toggle="tab" style="border-bottom: 0px solid %s">%s</a>
            </li>',$category->slug,get_field('color_de_la_categoria','category_'.$category->term_id),$category->name);
    }
	?>
</ul>
<div class="tab-content">
    <div id="todos" class="tab-pane fade in active">
        <?php
        foreach ($post_array as $post_data){
            $post_id = $post_data->ID;
            get_content(
                    get_the_post_thumbnail_url($post_id,'full'),
                    get_field('etiqueta_superior_grande','category_'.get_the_category($post_id)[0]->term_id),
                    $variable['color'],
                    strtoupper(get_the_category($post_id)[0]->cat_name),
                    get_the_permalink($post_id),$post_data->post_title);
        }
        ?>
    </div>
	<?php
	foreach ($categories as $category){
	    $post_array = get_posts($category->ID);
	    foreach ($post_array as $post_data){
		    $post_id = $post_data->ID;
	        echo '<div class="tab-pane fade" id="'.$category->slug.'">';
                get_content(
                        get_the_post_thumbnail_url($post_id,'full'),
	                    get_field('etiqueta_superior_grande','category_'.$category->term_id),
                        $variable['color'],$category->name,get_the_permalink($post_id),
                        $post_data->post_title);
            echo '</div>';
        }
	}
	?>
</div>

<?php
//AGREGAR OPCIONES DE ESTILO
Starterchild_Kirki::add_config('starter-child',array(
	'capability' => 'edit_theme_options',
	'option_type' =>'theme_mod'
));
Starterchild_Kirki::add_panel('custom_delisoya',array(
	'priority' =>10,
	'title' => __('Personalización Delisoya','starter-child'),
	'description'=>__('Campos personalizados para modificar la plantilla Delisoya'),
));
//FOOTER
Starterchild_Kirki::add_section('custom_footer',array(
	'title' => __('Footer Personalizado'),
	'description' => __('Sección para la personalización del footer'),
	'panel'=>'custom_delisoya',
	'priority' => 160,
	'capability' => 'edit_theme_options'
));
//FOOTER COLOR BG
Starterchild_Kirki::add_field('starter-child',array(
	'type' => 'color',
	'settings' => 'color_bg_footer',
	'label' => __('Color de las opciones del Footer','starter-child'),
	'section' =>'custom_footer',
	'default' => 'black',
	'choices' => array(
		'alpha' => true
	),
));
jQuery(document).ready(function( $ ){
    //TESTIMONIAL
    $(".owl-carousel").owlCarousel({
        loop:true,
        items:1,
        autoplay:true,
        autoplayHoverPause:true,
        responsive:{
            0:{
                margin:0
            },
            768:{
                margin: 15
            }
        }
    });
    $(window).on('load', function () {
        $('.loading').fadeOut();
    });
    //ASIGNACION DE BORDER A LOS LINK DEL BLOG
    var cat = $('.subtitle-cover h2').data('category');
    $('.category-sub-header a').each(function () {
        if($(this).data('category')===cat){
            $(this).addClass('active');
            var color = $(this).data('color');
            $(this).css('color',color);
        }
    });
});
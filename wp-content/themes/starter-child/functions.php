<?php
require_once 'include/include-kirki.php';
require_once 'include/class-starter-child-kirki.php';
require_once 'include/custom_theme_option.php';
require_once 'include/class-wp-delisoya-recent-posts.php';

//ELIMINAR MENSAJES DE COMPRAS
function remove_screenoptions() {
	echo '<style type="text/css">#setting-error-settings_updated { display: none; } </style>';
}
add_action('admin_head', 'remove_screenoptions');

//BORRAR COMPRAME EN EL CUSTOMIZER
function remove_buy() {
	echo '<style type="text/css">#accordion-section-pro_ads { display: none !important;} </style>';
}
add_action('customize_register', 'remove_buy');

//REGISTRAR NUEVOS WIDGETS
register_sidebars(2,array(
	'name' => 'Deli Bottom %d',
	'id' => 'deli-bottom',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>'));
//AGREGAR JS PERSONALIZADO
function add_theme_scripts() {
	wp_enqueue_script( 'script', get_stylesheet_directory_uri() . '/js/main.min.js', array ( 'jquery' ), 1.1, true);
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

//PONER EL FILTRO DE POST
function get_categorias($atts) {
	$variable = shortcode_atts(array(
		'color' => '#08255A',
	),$atts);
	ob_start();
	include locate_template('include/template/category_list.php',false,false);
	return ob_get_clean();
}
add_shortcode('filtro_post', 'get_categorias');

//TESTIMONIOS
function get_testimonial($atts) {
	$variable = shortcode_atts(array(
		'color' => 'black',
	),$atts);
	ob_start();
	include locate_template('include/template/testimonial.php',false,false);
	return ob_get_clean();
}
add_shortcode('testimonial', 'get_testimonial');

//AGREGAR PÁGINA DE OPCIONES
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Opciones Personalizables',
		'menu_title'	=> 'Opciones Personalizables',
		'menu_slug' 	=> 'custom-options',
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Testimonios',
		'menu_title'	=> 'Testimonios',
		'menu_slug' 	=> 'testimonio',
		'parent_slug'   => 'custom-options',
		'redirect'		=> false
	));
}

//AGREGAR EL FILTRO DE POST EN EL VISUAL COMPOSER
add_action('vc_before_init','get_filtro_post_vc');
function get_filtro_post_vc(){
	vc_map(array(
		'name' => __('Filtro de post','starter-child'),
		'base' => 'filtro_post',
		'category' => __('Delisoya','starter-child'),
		'params' => array(
			array(
				'type' => 'colorpicker',
				'heading' => __('Color de fondo','starter-child'),
				'param_name' => 'color',
				'value' => __('#08255A','starter-child')
			)
		)
	));
}
//AGREGAR los TESTIMONIOS
add_action('vc_before_init','get_testimonios_vc');
function get_testimonios_vc(){
	vc_map(array(
		'name' => __('Testimonios','starter-child'),
		'base' => 'testimonial',
		'category' => __('Delisoya','starter-child'),
		'params' =>array(
			array(
				'type' => 'colorpicker',
				'heading' => __('Color de text','starter-child'),
				'param_name' => 'color',
				'value' => __('#000','starter-child')
			)
		)
	));
}
//WIDGET MODIFICADO
add_action("widgets_init", "my_custom_widgets_init");
function my_custom_widgets_init(){
	register_widget("WP_Delisoya_Recent_Posts");
}
<!-- start footer -->

<footer id="footer" class="footer-wrap" style="background: <?php echo get_theme_mod('color_bg_footer','black')?>">
    <div class="container">
        <div class="bottom">
            <div class="row my-30">
                <div class="col-sm-4 flex jc-center fd-column">
                    <?php dynamic_sidebar('bottom'); ?>
                </div>
                <div class="col-sm-4 flex jc-center fd-column">
                    <?php dynamic_sidebar('deli-bottom-1'); ?>
                </div>
                <div class="col-sm-4 flex jc-center">
                    <?php dynamic_sidebar('deli-bottom-2'); ?>
                </div>
            </div>
        </div>
    </div> <!-- end container -->
</footer>
</div> <!-- #page -->
<?php wp_footer(); ?>
</body>
</html>

<article class="starter-border" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>   
    <?php if ( has_post_thumbnail() ){ ?>
        <div class="blog-details-img">
            <div class="col-sm-4">
	            <?php if( is_single() ){ ?>
		            <?php the_post_thumbnail('starterpro-large', array('class' => 'img-responsive')); ?>
	            <?php } else { ?>
                    <a href="<?php the_permalink(); ?>">
			            <?php the_post_thumbnail('starterpro-large', array('class' => 'img-responsive')); ?>
                        <img style="position: absolute;top:0;left: 30px" src="<?php echo get_field('etiqueta_superior_grande','category_'.get_the_category()[0]->term_id)?>">
                    </a>
	            <?php }?>
            </div>
        </div>
    <?php }  ?>

    <?php if( is_single() ){ ?>
        <?php get_template_part( 'post-format/entry-content' ); ?> 
    <?php }  ?>
    
    <div class="entry-blog">
        <div class="col-sm-8">
	        <?php if( !is_single() ){ ?>
		        <?php get_template_part( 'post-format/entry-content' ); ?>
	        <?php }  ?>
        </div>
     </div> <!--/.entry-meta -->
</article><!--/#post-->

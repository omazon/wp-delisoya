<?php
$category = get_the_category()[0];
printf('
    <span class="font-30 gotham-bold" style="color: %s;">%s</span>
    ',get_field('color_de_la_categoria','category_'.$category->term_id),$category->name);
?>
<div class="entry-header">
  <h2 class="entry-title blog-entry-title">
    <?php if ( is_single() ) {?>
        <?php the_title(); ?>
    <?php } else { ?>
    <a class="gotham-bold blue-text" href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
    <?php }?>
    <?php if ( is_sticky() && is_home() && ! is_paged()) { ?>
    <span class="featured-post"><i class="fa fa-star"></i></span>
    <?php } ?>
</h2> <!-- //.entry-title -->
</div>